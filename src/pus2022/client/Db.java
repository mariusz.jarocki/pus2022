package pus2022.client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Db {
    public Connection connection = null;
    public Db(String dbUrl) throws SQLException, ClassNotFoundException {
        if(connection == null) {
            connection = DriverManager.getConnection(dbUrl);
            Statement st = connection.createStatement();
            st.execute("CREATE TABLE IF NOT EXISTS lastseen (sender INTEGER,lastread INTEGER)");
        }
    }
}
