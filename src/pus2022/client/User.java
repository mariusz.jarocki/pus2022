package pus2022.client;

import java.util.Date;

public class User {
    private String login;
    private String email;
    private String uuid;
    private Date lastseen;
    public User(String login, String email, String uuid) {
        this.login = login;
        this.email = email;
        this.uuid = uuid;
        lastseen = new Date();
    }
    public String toString() {
        return login + " <" + email + ">";
    }
}
